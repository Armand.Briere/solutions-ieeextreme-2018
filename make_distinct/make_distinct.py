N, A = int(input()), sorted(list(map(int, input().split(' '))))
x, y = ((N-1)//2, round(sum(A)/N)) if (N%2) else ((N-1)/2, sum(A)//N+0.5)
print(sum([int(abs(xi-yi+(y-x))) for xi, yi in enumerate(A)]))
