"""
Troll Coder
Time limit: 2500 ms
Memory limit: 256 MB

Interactive
You have found a huge treasure on an island, but the only access point to that island is a bridge guarded by a giant Troll! In order to cross the bridge, you have to guess a sequence of NN bits by submitting queries. For each query, the Troll will tell you how many bits you guesses correctly until you guess the correct sequence.

Interaction
Your program must exchange information with the Troll by submitting queries and reading answers.

Note that you must flush the buffer so that the output reaches the Troll. Here we ilustrate it for several languages.

At the beginning of each test case, the Troll will give you a single integer NN which will represent the length of the sequence.

To submit a query, your program should output the letter Q followed by a space and by a binary sequence of length NN with each bit separated by a space. After each query you will receive an integer denoting the number of correct bits. The last submission will be your final answer and it should start with an A followed by a space and by a binary sequence of length NN with each bit separated by a space.

Constraints and notes
This task is NOT adaptive
1 \le N \le 1001≤N≤100
Your program can submit at most N + 1N+1 queries before arriving at the correct answer.

Ex:
6

Output:
Q 0 0 0 0 0 0
.
.
.
A 0 1 1 0 1 0
"""

import sys

how_many_number = int(input())

data = [0] * how_many_number


class Answer:

    def __init__(self, data_list):
        self.data = data_list
        self.guess = ""
        self.answer = 0
        self.previous_answer = 0
        self.last_modified = 0

    def generate_guess(self):
        guess = "Q"

        i = 0
        while i < how_many_number:
            guess += " " + str(self.data[i])
            i += 1

        self.guess = guess

    def check_guess(self):
        if int(self.answer) <= int(self.previous_answer):
            self.last_modified -= 1
            self.modify_value(0)

    def check_answer(self):
        if int(self.answer) != len(self.data):
            return False
        else:
            return True

    def generate_answer(self):
        self.answer = "A" + self.guess[1:]
        print(self.answer)
        sys.stdout.flush()

    def print(self):
        print(self.guess)
        sys.stdout.flush()

    def modify_value(self, value):
        self.data[self.last_modified] = value
        self.last_modified += 1

    def do_it_for_me(self):
        self.modify_value(1)
        self.generate_guess()
        self.print()
        self.previous_answer = self.answer
        self.answer = input()
        self.check_guess()


answer = Answer(data)
answer.generate_guess()
answer.print()
answer.answer = input()

while not answer.check_answer():
    answer.do_it_for_me()
    answer.check_answer()

answer.generate_answer()
