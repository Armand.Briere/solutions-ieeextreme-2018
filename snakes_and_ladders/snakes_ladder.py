#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import sys

def coords_to_pos(x, y, board_size):
    pos = board_size * (y-1)
    if y % 2 != 0:
        pos += x
    else:
        pos += board_size - x + 1
    return pos

def pos_to_coords(pos, board_size):
    y = (pos - 1) // board_size + 1
    if y % 2 != 0:
        x = pos - (y-1) * board_size
    else:
        x = board_size - (pos - (y-1) * board_size) + 1
    return x, y

class BoardPosition():
    def __init__(self, position_num, board_size):
        self.pos = position_num
        self.board_size = board_size

    def get_pos(self):
        return self.pos

    def get_coords(self):
        return (pos_to_coords(self.pos, self.board_size))

    def move_dice(self, roll):
        self.pos += roll

    def go_to_coords(self, x, y):
        self.pos = coords_to_pos(x, y, self.board_size)

    def has_won(self):
        return self.pos >= self.board_size ** 2 + 1

def play_turn(roll, player_turn, player_list):
    i = 0
    while player_list[(player_turn + i) % len(player_list)].has_won():
        i += 1
        if i == len(player_list):
            return None, None
    player_list[(player_turn + i) % len(player_list)].move_dice(roll)
    next_turn = (player_turn + i + 1) % len(player_list)
    return (player_turn + i) % len(player_list), next_turn


board_size = int(sys.stdin.readline().strip("\n"))
num_players = int(sys.stdin.readline().strip("\n"))
num_snakes = int(sys.stdin.readline().strip("\n"))

snakes = {}
for i in range(num_snakes):
    snake = [int(x) for x in sys.stdin.readline().strip("\n").split(" ")]
    snakes[(snake[0], snake[1])] = (snake[2], snake[3])

num_ladders = int(sys.stdin.readline().strip("\n"))
ladders = {}
for i in range(num_ladders):
    ladder = [int(x) for x in sys.stdin.readline().strip("\n").split(" ")]
    ladders[(ladder[0], ladder[1])] = (ladder[2], ladder[3])

snakes.update(ladders)

num_rolls = int(sys.stdin.readline().strip("\n"))

players = [BoardPosition(0, board_size) for _ in range(num_players)]

next_player = 0
for i in range(num_rolls):
    roll = sys.stdin.readline().strip("\n").split(" ")
    roll = int(roll[0]) + int(roll[1])
    player_played, next_player = play_turn(roll, next_player, players)
    if player_played is None:
        break
    while players[player_played].get_coords() in snakes:
        players[player_played].go_to_coords(*(snakes[players[player_played].get_coords()]))

for i in range(len(players)):
    if players[i].has_won():
        print("{} winner".format(i + 1))
    else:
        print("{} {} {}".format(i + 1, *players[i].get_coords()))
