#!/usr/bin/env python3
import sys


Tree = {}
Score = {}

def find(start, end, prize, visited = []):
    if start in Score:
        Score[start] += prize
    else:
        Score[start] = prize
    for node in Tree[start]:
        if node == end:
            if end in Score:
                Score[end] += prize
            else:
                Score[end] = prize
            return True
    visited.append(start)
    for node in Tree[start]:
        if node not in visited:
            if find(node, end, prize, visited):
                return True
    return False


input_data = sys.stdin.readline().strip('\n').split(' ')
num_nodes, num_operations = [int(x) for x in input_data]
for i in range(num_nodes - 1):
    nodes = [int(x) for x in sys.stdin.readline().strip('\n').split(' ')]
    try:
        Tree[nodes[0]].append(nodes[1])
    except KeyError:
        Tree[nodes[0]] = [nodes[1],]
    try:
        Tree[nodes[1]].append(nodes[0])
    except KeyError:
        Tree[nodes[1]] = [nodes[0],]

for i in range(num_operations):
    operation =  [int(val) for val in sys.stdin.readline().strip('\n').split(' ')]
    find(operation[0], operation[1], operation[2])

maxi = 0

for key, score in Score.items():
    maxi = max(score, maxi)
print(maxi)
