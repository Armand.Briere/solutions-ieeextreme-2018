from collections import deque, defaultdict
from functools import lru_cache

@lru_cache(maxsize=None)
def breadth_first_search(start, goal):
    frontier = deque([start])
    came_from = {start: None}
    found = False
    while len(frontier):
        current = frontier.popleft()
        if current == goal:
            found = True

        for adj in nodes[current]:
            if adj not in came_from:
                frontier.append(adj)
                came_from[adj] = current

    return (came_from, found)

def update_scores(start, goal, parents, found):
    if found:
        scores[goal] += K
        current = parents.get(goal)
        while current is not None:
            scores[current] += K
            current = parents[current]
    else:
        scores[start] += K

nodes = defaultdict(list)
scores = defaultdict(int)

N, M = list(map(int, input().split(' ')))
for edge in range(N-1):
    U, V = list(map(int, input().split(' ')))
    nodes[U].append(V)
    nodes[V].append(U)

for operation in range(M):
    A, B, K = list(map(int, input().split(' ')))
    parents, found = breadth_first_search(A, B) if (A in nodes and B in nodes) else ({}, False)
    update_scores(A, B, parents, found)

print(max(scores.values()))
